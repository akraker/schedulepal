# SchedulePal

www.schedulepal.com

generated using Luminus version "2.9.10.31"

## Hofstadter's Law

*"It always takes longer than you expect, even when you take into account
Hofstadter's Law"*

*- Douglas Hofstadter*

## Essential Reading

### Time is hairy; here's why:

http://infiniteundo.com/post/25326999628/falsehoods-programmers-believe-about-time

https://news.ycombinator.com/item?id=4128208

http://naggum.no/lugm-time.html

### Clojure Style Guide

https://github.com/bbatsov/clojure-style-guide

"Functions are dash-separated-lowercase-words, ideally descriptively chosen so
that your code is clear and self-documenting."

http://stackoverflow.com/questions/6709131/what-are-clojures-naming-conventions

## Prerequisites

You will need [Leiningen][1] 2.0 or above installed.

[1]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein run

To view the app browse to:
	
	localhost:3000

Connect to nREPL on port:

	7000

## Postgres Database Set-up

Install PostgreSQL, initialize database, and enable the postresql.service

See https://wiki.archlinux.org/index.php/PostgreSQL for details.

### Set-up schedulepal user role, password, authorization, and scope:

Launch psql

    psql -U postgres -d postgres -h localhost

Create schedulepal database role

    postgres=# CREATE ROLE schedulepal LOGIN;

Set password

    postgres=# \password schedulepal;
    password: sch3dul3p4l

Create schedulepal schema and grant authorization

    postgres=# CREATE SCHEMA AUTHORIZATION schedulepal;
    postgres=# GRANT ALL ON SCHEMA schedulepal TO schedulepal;
    postgres=# GRANT ALL ON ALL TABLES IN SCHEMA schedulepal TO schedulepal;

Limit the scope of schedulepal by defining its search path

    postgres=# ALTER USER schedulepal SET search_path = 'schedulepal';

Exit PostgreSQL shell

    postgres=# \q

### Check set-up:

Login as schedulepal:

    psql -U schedulepal -d postgres -h localhost
    postgres=# \conninfo
    postgres=# \dn

### make db profile:

From project folder:

    touch profiles.clj

Then put something like this in there to reflect the database role and password
you set up:

    {:profiles/dev  {:env {:database-url "jdbc:postgresql://localhost/postgres?user=schedulepal&password=sch3dul3p4l"}}
     :profiles/test  {:env {:database-url "jdbc:postgresql://localhost/postgres?user=schedulepal&password=sch3dul3p4l"}}}

### Migrate database schema:

	lein migratus migrate

See http://www.luminusweb.net/docs/migrations.md for details.

### Check migration:

Login to database as schedulepal:

	psql -U schedulepal -d postgres -h localhost

Check database tables:

	postgres=# \dt

## License

Copyright © 2016 FIXME
