(ns schedulepal.db.spal
  (:require clj-time.jdbc
            [conman.core :as conman]
            [schedulepal.db.core :as db]
            [schedulepal.util :as util]))

(conman/bind-connection db/*db* "sql/spal/events.sql")

(defn process-spevent
  "Given a SchedulePal Event, process it and insert it into the DB.

  TODO: Expand this so it handles all of the potential spal event fields."
  [{:keys [:event/duration
           :event/dur-unit
           :event/enddate
           :event/endtime
           :event/endtimezone
           :event/startdate
           :event/starttime
           :event/starttimezone
           :event/summary]}]
  (let [{st-str :str st-hours :hours st-mins :minutes st-ampm :ampm} starttime
        {e-str :str e-hours :hours e-mins :minutes e-ampm :ampm} endtime]
    (try
      (insert-spevent {:duration duration :dur_unit dur-unit :summary summary
                       :startdate startdate :starttimestr st-str :starttimehours st-hours
                       :starttimemins st-mins :starttimeampm st-ampm :starttimezone starttimezone
                       :enddate enddate :endtimestr e-str :endtimehours e-hours :endtimemins e-mins
                       :endtimeampm e-ampm :endtimezone endtimezone})
      (catch Exception e
        (prn (.getNextException e))))))
