(ns schedulepal.db.google
  (:require clj-time.jdbc
            [conman.core :as conman]
            [schedulepal.db.core :as db]
            [schedulepal.util :as util]))

(conman/bind-connection db/*db* "sql/google.sql")

(defn process-gevent
  "Given a Google Event, process it and insert it into the DB.

  TODO: Expand this so it handles all of the potential google event fields."
  [{:keys [:gevent/etag :gevent/id :gevent/created
           :gevent/summary :gevent/description :gevent/location
           :gevent/start :gevent/end]}]
  (let [startzone (get start :timeZone)
        startstamp (util/to-timestamp-no-tz (get start :dateTime))
        endzone (get end :timeZone)
        endstamp (util/to-timestamp-no-tz (get end :dateTime))
        createdstamp (util/to-timestamp-no-tz created)]
    (try
      (insert-gevent {:etag etag :id id :created createdstamp
                      :summary summary :description description :location location
                      :startdatetime startstamp :startzone startzone
                      :enddatetime endstamp :endzone endzone})
      (catch Exception e
        (prn (.getNextException e))))))
