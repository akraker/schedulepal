(ns schedulepal.db.user-model
  (:require [yesql.core :refer [defqueries]]
            [schedulepal.auth :as auth]))

(def db-spec {:classname    "org.postgresql.Driver" ;#1
              :subprotocol  "postgresql"
              :subname      "//localhost/postgres"
              :user         "schedulepal"
              :password     "sch3dul3p4l"})

(defqueries "sql/users.sql"
             {:connection db-spec})                 ;#2

(defn add-user!
  "Saves a user to the database."
  [user]
  (let [new-user (->> (auth/encrypt-password (:password user))
                      (assoc user :password)
                      insert-user<!)] 
    (dissoc new-user :pass)))
