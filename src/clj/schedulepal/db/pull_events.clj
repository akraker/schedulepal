(ns schedulepal.db.pull-events
  (:require [yesql.core :refer [defqueries]]))

(def db-spec {:classname    "org.postgresql.Driver" ;#1
              :subprotocol  "postgresql"
              :subname      "//localhost/postgres"
              :user         "schedulepal"
              :password     "sch3dul3p4l"})

(defqueries "sql/pull_events.sql"
             {:connection db-spec})                 ;#2

(defn get-user-events
  "Gets user events from the database."
  []
  (get-event-data))

;;for reference
#_(defn add-user!
  "Saves a user to the database."
  [user]
  (let [new-user (->> (auth/encrypt-password (:password user))
                      (assoc user :password)
                      insert-user<!)] 
    (dissoc new-user :pass)))
