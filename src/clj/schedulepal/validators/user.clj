(ns schedulepal.validators.user
  (:require [bouncer.core :as b]
            [bouncer.validators :as v]))

; Test map
(def signup {:username  "TheDude" 
             :email     "thedude@bides.net" 
             :password  "123456"})

(defn signup-valid? [signup]
  "Validates the incoming map of values from our signup form,
  and returns a boolean. Expects signup to have :username, 
  :email, and :password."
  (b/valid? signup
    :username v/required
    :email    v/email
    :password [v/required [v/min-count 8]]))

(defn validate-signup [signup]
 "Validates the incoming map of values from our signup form,
  and returns a map of errors or key value pairs depending.
  Expects to have :username, :email, and :password." 
  (b/validate signup
    :username v/required
    :email    v/email
    :password [v/required [v/min-count 8]]))
