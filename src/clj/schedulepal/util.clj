(ns schedulepal.util
  (:require [clj-time
             [coerce :as tc]
             [core :as t]
             [format :as tf]]))

(def rfc3339-datetime
  "Y-MM-dd'T'HH:mm:ssZZ")

(defn to-datetime
  "Takes in a string formatted to the rfc3339 full DateTime spec, and a long
  form timezone, i.e 'America/Chicago', and parses it into a DateTime object"
  [timestr tmz]
  (let [formatter (tf/formatter rfc3339-datetime)
        zoned-fmtr (tf/with-zone formatter (t/time-zone-for-id tmz))]
    (tf/parse zoned-fmtr timestr)))

(defn to-timestamp-no-tz
  "clj-time uses the built-in java.sql.Timestamp class, which converts
  org.joda.time.DateTime objects into UTC sql timestamps. This means we lose
  the timezone data, so we need store that separately."
  [datetime]
  (tc/to-timestamp datetime))
