(ns schedulepal.auth
  (:require [buddy.hashers :as hashers]))

(defn encrypt-password [password]
  "Expects the raw password."
  (hashers/encrypt password))

(defn check-password [raw encrypted]
  "Expects the raw password and the encrypted password."
  (hashers/check raw encrypted))
