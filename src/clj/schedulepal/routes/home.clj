(ns schedulepal.routes.home
  (:require [schedulepal.layout :as layout]
            [compojure.core :refer :all]
            [ring.util.response :as response]
            [clojure.java.io :as io]
            [schedulepal.db.user-model :as u]
            [schedulepal.validators.user :as v]))

(def user {:username "testuser" 
           :email "testuser@email.com"
           :password "12345678"})

(defn home-page []
  (layout/render
    "home.html" {:docs (-> "docs/docs.md" io/resource slurp)}))

(defn about-page []
  (layout/render "about.html"))

(defn signup-page []
  (layout/render "signup.html"))

(defn signup-page-submit [user]
  "Validate user registration in-put from signup page and insert into
  database or render signup page with error messages upon failure. 
  Expects to recieve a map containing :username, :email, and :password."
  (if (v/signup-valid? user)
    (do
      (u/add-user! user)
      (response/redirect "/signup-success"))
    ;;To-do: render signup template with error messages from bouncer. 
    (str "errors:  " (v/validate-signup user))))

(defroutes home-routes
  (GET  "/"       []        (home-page))
  (GET  "/about"  []        (about-page))
  (GET  "/signup" []        (signup-page))
  (POST "/signup" [& form]  (signup-page-submit form))
  (GET  "/signup-success" [] "Success!"))
