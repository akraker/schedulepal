(ns schedulepal.handler
  (:require [compojure.core :refer [defroutes routes wrap-routes]]
            [schedulepal.layout :refer [error-page]]
            [schedulepal.routes.home :refer [home-routes]]
            [schedulepal.routes.services :refer [service-routes]]
            [schedulepal.routes.test-routes :refer [test-routes]]
            [compojure.route :as route]
            [schedulepal.middleware :as middleware]
            [ring.middleware.cors :refer [wrap-cors]]))

(def app-routes
  (routes
   (wrap-cors #'test-routes
              :access-control-allow-origin [#"http://localhost:.*"]
              :access-control-allow-methods [:get :put :post :delete])
   (wrap-cors #'service-routes
              :access-control-allow-origin [#"http://localhost:.*"]
              :access-control-allow-methods [:get :put :post :delete])
    #'home-routes
    ;;(wrap-routes #'home-routes middleware/wrap-csrf)
    ;;TODO: Add csrf before deployment!
   (route/not-found
    (:body
     (error-page {:status 404
                  :title "page not found"})))))

(def app (middleware/wrap-base #'app-routes))
