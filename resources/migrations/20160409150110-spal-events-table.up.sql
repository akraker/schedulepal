CREATE TABLE spal_events (
        duration       int,
        dur_unit       text,
        summary        text,
        startDate      date,
        startTimeStr   text,
        startTimeHours int,
        startTimeMins  int,
        startTimeAmPm  text,
        startTimeZone  text,
        endDate        date,
        endTimeStr     text,
        endTimeHours   int,
        endTimeMins    int,
        endTimeAmPm    text,
        endTimeZone    text
);
