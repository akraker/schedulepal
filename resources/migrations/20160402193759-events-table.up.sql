CREATE TABLE user_events (
        kind           text,
        etag           text,
        id             text,
        htmlLink       text,
        created        timestamp with time zone,
        updated        timestamp with time zone,
        summary        text,
        description    text,
        location       text,
        startDate      date,
        startDateTime  time with time zone,
        startTimeZone  text,
        endDate        date,
        endDateTime    time with time zone,
        endTimeZone    text,
        recurrence     text
);
