-- :name pull-gevents :? :*
-- :doc Give us all of the Google Events
select * from user_events

-- :name insert-gevent :! :n
-- :doc Insert a Google Event into the User Events table
insert into user_events
        (etag, id, created, summary, description,
	location, startdatetime, starttimezone,
	enddatetime, endtimezone)
values (:etag, :id, :created, :summary, :description,
        :location, :startdatetime, :startzone,
	:enddatetime, :endzone)

-- :name drop-events! :! :n
delete from user_events
