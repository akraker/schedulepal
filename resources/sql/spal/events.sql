-- :name pull-spevents :? :*
-- :doc Give us all of the Google Events
select * from spal_events

-- :name insert-spevent :! :n
-- :doc Insert a SchedulePal Event into the User Events table
insert into spal_events
        (duration, dur_unit, summary,
	startdate, starttimestr, starttimehours, starttimemins, starttimeampm, starttimezone,
	enddate, endtimestr, endtimehours, endtimemins, endtimeampm, endtimezone)
values (:duration, :dur_unit, :summary,
        :startdate, :starttimestr, :starttimehours, :starttimemins, :starttimeampm, :starttimezone,
        :enddate, :endtimestr, :endtimehours, :endtimemins, :endtimeampm, :endtimezone)

-- :name drop-events! :! :n
delete from spal_events
