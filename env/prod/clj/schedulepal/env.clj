(ns schedulepal.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[schedulepal started successfully]=-"))
   :middleware identity})
