(ns user
  (:require [mount.core :as mount]
            schedulepal.core))

(defn start []
  (mount/start-without #'schedulepal.core/repl-server))

(defn stop []
  (mount/stop-except #'schedulepal.core/repl-server))

(defn restart []
  (stop)
  (start))


